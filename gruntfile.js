//Start by exporting module
module.exports = function(grunt){
  /***** Loading Tasks *****/
  grunt.loadNpmTasks('grunt-contrib-jshint'); //Jshint Task
  grunt.loadNpmTasks('grunt-contrib-uglify'); //Uglify Task
  grunt.loadNpmTasks('grunt-contrib-watch'); //Watch Task
  grunt.loadNpmTasks('grunt-contrib-compass'); //Compass task
  //Init the grunt configuration
  grunt.initConfig({
     //Jshint
     jshint : {
       all :"app/js/*.js"
     },
     //Uglify
     uglify : {
        options: {
          mangle: false
        },//Mangle will avoid changing the var name in angular js
        main :{
          files: {
            'public/js/main.min.js': 'app/js/main.js'
          }//files to minify
        },//Master js file
        controllers: {
          files: {
            'public/js/controllers.min.js': 'app/js/controllers.js'
          }//files to minify
        },//controllers
        directives : {
          files: {
            'public/js/directives.min.js': 'app/js/directives.js'
          }//files to minify
        },//directives
        services : {
          files: {
            'public/js/services.min.js': 'app/js/services.js'
          }//files to minify
        }//services
     },//uglify
     //Compass
     compass: {
       dev:{
         options: {
           config: 'config.rb'
         }
       }
     },
     //Watch
     watch : {
       options : {
         livereload : true
       },//Live reload option
       js : {
         files : "app/js/*.js",
         tasks : ["jshint","uglify"]
       },//check for errors then minify
       sass: {
         files : "app/sass/*.scss",
         tasks : "compass:dev"
       },
       html : {
         files : "public/*.html"
       }
     }//Watch
  });
  grunt.registerTask('default','watch');
}
