var directives = angular.module('directives',[]);

directives.directive('contactCard',function(){
  return {
    template : '<div class="contact-card">'+'<p>Name: {{contact.name}}</p>'+
                '<p>country: {{contact.country}}</p>'+'</div>',
    replace : false,
    scope : {
      'contact' : '=contactCard'
    },
    link : function($scope,$element,$attr){

    }
  };
});

directives.directive('datepicker',function(){
  return {
    require : 'ngModel',
    link: function($scope,$element,$attr,ngModelCtrl){
      console.log(ngModelCtrl);
      var isInit = false;
      $attr.$observe('datepickerFormat',function(newValue){
        if(isInit){
          $element.datepicker('option', 'dateFormat', newValue);
        }else if(newValue){
          $element.datepicker({
            dateFormat : newValue,
            onSelect : function(date){
              $scope.$apply(function(){
                ngModelCtrl.$setViewValue(date);
              });
            }
          });
          isInit = true;
        }
      });
      ngModelCtrl.$render = function(){
        $element.datepicker('setDate', ngModelCtrl.$viewValue);
      };
    }
  };
});


directives.directive('wysiwyg',function(){
  return {
    require : 'ngModel',
    link : function($scope,$element,$attr,ngModelCtrl){
        //Init hallo
        $element.hallo({
          plugins : {
            'halloformat' : {},
            'halloblock' : {},
            'hallojustify' : {},
            'hallolists' : {},
            'halloreundo' : {}
          }
        });
        ngModelCtrl.$render = function(){
          var contents = ngModelCtrl.$viewValue;
          $element.hallo('setContents',contents);
        };
        var converter = new showdown.Converter();
        var formater = function(markdown){
          return converter.makeHtml(markdown);
        };
        ngModelCtrl.$formatters.push(formater);

        var parser = function(html){
          return toMarkdown(html);
        };
        ngModelCtrl.$parsers.push(parser);

        $element.on('hallomodified',function(){
          var contents = $element.hallo('getContents');
          ngModelCtrl.$setViewValue(contents);
          $scope.$digest();
        });
    }
  };
});


directives.directive('uniqueCheck',['uniquness',function(uniquness){
    return {
      require : 'ngModel',
      link : function($scope,$element,$attr,ngModelCtrl){
        var checkUnique = function(name){
          var isValid = !uniquness.taken(name);
          ngModelCtrl.$setValidity('unique',isValid);
          
          return name;
        };
        ngModelCtrl.$parsers.push(checkUnique);
      }
    };
}]);
