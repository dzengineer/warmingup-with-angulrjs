var controllers = angular.module('controllers',[]);

controllers.controller('appCtrl',['$scope','$http','people','uniquness',
                                 function($scope,$http,people,uniquness){
  $scope.test = "test";

  $scope.people = people.get();

  $scope.add = function(person){
    people.add(person);
  };
  $scope.edit = function(person){
    $scope.person = person;
  };
  $scope.delete = function(person){
    people.delete(person);
  };

   $http.get('markdown.md').success(function(data){
     $scope.bio = data;
   });


}]);
