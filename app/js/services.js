var services = angular.module('services',[]);

  services.factory('people',['$http',function($http){
    var people = [];
    return {
      get : function(){
          if(!people.length){

            $http.get('people.json')
                 .then(function(res){
                   for (var i = 0; i < res.data.length; i++) {
                     people.push(res.data[i]);
                   }//Adding values to people array
                 });
          }//Check if the people array is empty
          return people;

      },
      add : function(person){
          people.push(person);
      },
      delete: function(person){
        people.splice(people.indexOf(person),1);
      }
    };
  }]);


  services.factory('uniquness',['$http',function($http){
    var usernames = [];
    return {
      taken : function(name){
        if(usernames.length === 0){
          $http.get('users.json').success(function(res){
            for (var i = 0; i < res.users.length; i++) {
              usernames.push(res.users[i]);
            }
          });
        }
        return usernames.indexOf(name) >= 0;
      }
    };
  }]);
