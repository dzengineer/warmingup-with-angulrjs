#Compass config file
require 'susy' #Include susy framework
css_dir = 'public/css' #Processed Css folder
sass_dir = 'app/sass' #Sass Folder
javascript = 'public/js' #Javascript folder
output_style = :expanded #Expanded style for debuging purposes
